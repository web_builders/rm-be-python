# -*- coding: utf-8 -*-
"""
This file belongs to Web Builder, developed by Ramón Mariño Solís.
The goas is to automate the process of getting the confirmation about whether a client has an specific request in the CRM web.
"""

from selenium import webdriver
from msedge.selenium_tools import Edge, EdgeOptions
from selenium.webdriver.edge.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd

def click_emelent_by_id(browser, element_id):
    browser.find_element_by_id(element_id).click()
    
def click_by_partial_link(browser,link_element):
    try:
        WebDriverWait(browser, 3).until(
           EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, link_element))
    		)
    finally:
        browser.find_element_by_partial_link_text('Contactos').click()
        
def get_element_by_id(browser, element_id):
    try:
        element = WebDriverWait(browser, 3).until(
            EC.presence_of_element_located((By.ID, element_id))
		)
    finally:
        element = browser.find_element_by_id(element_id)
    return element
#La función lleva al webdriver hacia la página del cliente, se loguea y navega hasta la página dónde hay que pasar los datos.
def navigate_to_customer_searcher(username, password, browser):
    browser.get('https://{}:{}@ecanal.telefonica.es/w1/avisoLegal'.format(username,password))
    browser.get('https://{}:{}@ecanal.telefonica.es/w1/home'.format(username,password))
    browser.switch_to.frame("W1DefaultW1")
    browser.execute_script("javascrit:top.frames['W1CabPagW1'].abrirNavegador('buscar')")

#Función para bloquear unos segundos de tiempo para que el navegador acabe de cargar la página
def wait(browser, seconds):    
    browser.implicitly_wait(seconds)
#Función que introduce los datos pasados en el fichero Excel y los envía para pasar a la siguiente ventana
def sent_customer_data(browser, tipoDocumento, numeroDocumento):
    try:
        WebDriverWait(browser, 9).until(
            EC.presence_of_element_located((By.ID,'formCliente'))
            )
    finally:
        form = browser.find_element_by_id('formCliente')
        _Contexto_id = get_element_by_id(form, "_Contexto_id")
        _Contexto_id.send_keys('Movil')
        _Tipodocumento_id = get_element_by_id(form,"_Tipodocumento_id")
        _Tipodocumento_id.send_keys(tipoDocumento) 
        _NumeroDocumento_id = form.find_element_by_id("widget__Numerodocumento_id").find_element_by_id("_Numerodocumento_id")
        _NumeroDocumento_id.send_keys(numeroDocumento)
    
        click_emelent_by_id(browser, 'botonBuscar')
        

#Se recogen los datos de usuario y contraseña introducidos por el cliente
username, password, path= input('Give me the username, password and file path\n').split()
#Se inicia el webdriver instalado en el ordenador del cliente en el path dado
#browser = webdriver.Edge(r'C:\Users\admin\Desktop\edgedriver_win32\msedgedriver.exe')
options = EdgeOptions()
options.use_chromium = True
options.add_argument("headless")
options.add_argument("disable-gpu")
browser = Edge(r'C:\Users\admin\Desktop\edgedriver_win32\msedgedriver.exe', options= options)
#path = 'C:/Users/admin/OneDrive/Documents/rm-be-python/lista_CIFs.xlsx'
lista_CIF = pd.read_excel(path)

#Workflow. El program en sí empieza ahora.
#Las siguientes líneas 
navigate_to_customer_searcher(browser=browser, username=username, password=password)
wait(browser, 3)

for row_index, row in lista_CIF.iterrows():
    print('Procesando el {} con número {}\n'.format(row[0], row[1]))
    sent_customer_data(browser=browser, tipoDocumento=row[0], numeroDocumento=row[1])
    wait(browser, 6)
    
    customerDetails = get_element_by_id(browser, 'formPopupAlarmas')
    detalles = customerDetails.text
    inicio_detalles = detalles.find('Descripción Enlace\n\n\n\n-')+len('Descripción Enlace\n\n\n\n-')
    final_detalles = detalles.find('-\nMensajes\n')
    
    click_emelent_by_id(browser,"botonAceptarAlarmas")
    click_by_partial_link(browser, 'Contactos')
    tabla_contactos = get_element_by_id(browser, 'tablaContactos_wrapper')
    wait(browser, 10)    
    if tabla_contactos.text.find('ÑÑÑGevico - encuesta de Gevico por email') == -1:
        answer = 'No'
    elif tabla_contactos.text.find('ÑÑÑGevico - encuesta de Gevico por email') != -1:
        answer = 'Yes'
        
    lista_CIF.at[row_index, 'Búsqueda encontrada'] = answer
    lista_CIF.at[row_index, 'Detalles cliente'] = detalles[inicio_detalles:final_detalles]
    
    navigate_to_customer_searcher(browser=browser, username=username, password=password)
    print('El {} con número {} ha sido procesado\n'.format(row[0], row[1]))

print('Se han procesado todos los clientes. El archivo ya está actualizado con los datos')
#Se exportan los datos 
lista_CIF.to_excel(path, index=False)
browser.close()