﻿# -*- coding: utf-8 -*-
"""
This code belongs to Web Builders company. 
The use of this code without explicit permission of this entity could carry legal consequences.
Developed by Ramón Mariño Solís behalf of Web Builders..
python 3.8 
"""

from msedge.selenium_tools import Edge, EdgeOptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
import sys

def check_file(file):
    try:
        columns = file.columns
        if columns.isin(['Tipo documento','Número de documento']).sum() == 2:
            print('El fichero tiene el formato correcto')
            check = 1    
        else:
            check = 0
            print("""
                  El fihcero no contiene alguna de las columnas: Tipo documento o Número de documento \n 
                  Por favor, revise el fichero y añada el nombre indicado a las columnas\n 
                  Gracias!
                  """)  
        return check
    except :
        e = sys.exc_info()[0]
        return e

def click_element_by_id(browser, element_id):
    try:
       WebDriverWait(browser, 9).until(
           EC.element_to_be_clickable((By.ID, element_id))
    		)
    finally:
        browser.find_element_by_id(element_id).click()
       
def click_by_partial_link(browser,link_element):
    try:
        WebDriverWait(browser, 9).until(
           EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, link_element))
    		)
    finally:
        browser.find_element_by_partial_link_text('Contactos').click()

def export_new_file(file, path):
    file_name = path[path.rfind('\\')+1:]
    new_file_name = file_name[:file_name.find('.')]+'_detallado'+file_name[file_name.find('.'):]
    new_path = path[:path.rfind('\\')+1]+new_file_name
    file.to_excel(new_path, index=False)
    
def get_element_by_id(browser, element_id):
    try:
        element = WebDriverWait(browser, 9).until(
            EC.presence_of_element_located((By.ID, element_id))
		)
    finally:
        element = browser.find_element_by_id(element_id)
    return element

def get_customer_details(element):
    try:
        full_detalles = element.text    
        inicio_detalles = full_detalles.find('Indicadores')+len('Indicadores\nObserv Descripción Enlace\n')+1
        final_detalles = full_detalles.find('Mensajes\n')
        detalles = full_detalles[inicio_detalles:final_detalles]
    except:
        detalles = 'Error leyendo los indicadores del cliente'
    if detalles == """nsajes
No se encuentran datos
El cliente no tiene mensajes asociados.
Notas
Acepta""":
        detalles = 'El cliente no tiene indicadores asociados'
    return detalles, full_detalles

#La función lleva al webdriver hacia la página del cliente, se loguea y navega hasta la página dónde hay que pasar los datos.
def navigate_to_customer_searcher(username, password, browser):
    try:
        browser.get(f'https://{username}:{password}@ecanal.telefonica.es/w1/avisoLegal')
        browser.get(f'https://{username}:{password}@ecanal.telefonica.es/w1/home')        
        try:
            WebDriverWait(browser, 9).until(
                EC.frame_to_be_available_and_switch_to_it((By.NAME, 'W1DefaultW1'))
        		)
        finally:   
            browser.execute_script("javascrit:top.frames['W1CabPagW1'].abrirNavegador('buscar')")
    except:
        print('Problemas con el servidor, por favor pruebe más tarde')

#Esta funcion recoge los detalles del cliente y os separa en columnas. Una columna por fila obtenida
def split_customer_details(file):
    
    detalles_cliente = file['Detalles cliente'].str.split('-\n-', expand=True)
    n_col = detalles_cliente.shape[1]
    
    column_names = []
    for i in range(n_col):
        column_names.append(f'detalle_{i}')    
        
    detalles_cliente.columns = column_names
    detalles_cliente['Número de documento'] = file['Número de documento']
    file = file.merge(detalles_cliente, on='Número de documento')
    file = file.drop(columns='Detalles cliente')
    file['detalle_0'].fillna(value='El cliente no tiene indicadores especificados', inplace=True)
    file.fillna(value=' ', inplace=True)
    file.replace(to_replace='-', value='', inplace=True)
    return file

#Función que introduce los datos pasados en el fichero Excel y los envía para pasar a la siguiente ventana
def sent_customer_data(browser, tipoDocumento, numeroDocumento):
    try:
        WebDriverWait(browser, 9).until(
            EC.visibility_of_element_located((By.ID,'formCliente'))
            )
    finally:
        form = browser.find_element_by_id('formCliente')
        _Contexto_id = get_element_by_id(form, "_Contexto_id")
        _Contexto_id.send_keys('Movil')
        _Tipodocumento_id = get_element_by_id(form,"_Tipodocumento_id")
        _Tipodocumento_id.send_keys(tipoDocumento) 
        _NumeroDocumento_id = form.find_element_by_id("widget__Numerodocumento_id").find_element_by_id("_Numerodocumento_id")
        _NumeroDocumento_id.send_keys(numeroDocumento)
    
        click_element_by_id(browser, 'botonBuscar')

#Se recogen los datos de usuario y contraseña introducidos por el cliente
#print('Argument List:', str(sys.argv))
username, password, path = sys.argv[1], sys.argv[2], sys.argv[3]

#Se inicia el webdriver instalado en el ordenador del cliente en el path dado
options = EdgeOptions()
#options.use_chromium = True
#options.add_argument("headless")
#options.add_argument("disable-gpu")
browser = Edge(r'C:\xampp\htdocs\rm-be-python\msedgedriver.exe', options= options)

"""
Se crea un dataframe con los datos proporcionados por el cliente.
Luego se verifica que tenga las columnas necesarias para que funcione el programa.
Finalmente se incluyen las nuevas columnas con el duata type correcto
"""
#Leemos el fichero y forzamos el dtype a string(object) para evitar errores al cargar los datos.
lista_CIF = pd.read_excel(path)
lista_CIF = lista_CIF.astype('object')

if check_file(lista_CIF) == 1:
    
    #Execute Workflow. El program en sí empieza ahora.
    #Las siguientes líneas 
    navigate_to_customer_searcher(browser=browser, username=username, password=password)
    
    for row_index, row in lista_CIF.iterrows():
        print('Procesando el {} con número {}'.format(row['Tipo documento'], row['Número de documento']))
        sent_customer_data(browser=browser, tipoDocumento=row['Tipo documento'], numeroDocumento=row['Número de documento'])
        try:
            WebDriverWait(browser, 10).until(EC.visibility_of_element_located((By.ID,'formPopupAlarmas')))          
            customerDetails = get_element_by_id(browser, 'formPopupAlarmas')
            detalles, full_detalles = get_customer_details(customerDetails)
            click_element_by_id(browser,"botonAceptarAlarmas")            
            click_by_partial_link(browser, 'Contactos')            
            tabla_contactos = get_element_by_id(browser, 'tablaContactos_wrapper')

            if tabla_contactos.text.find('ÑÑÑGevico - encuesta de Gevico por email') == -1:
                answer = 'No'
            elif tabla_contactos.text.find('ÑÑÑGevico - encuesta de Gevico por email') != -1:
                answer = 'Si'          
        except:
            detalles = """Error: El cliente no se ha encontrado en la base de datos. Por favor, revise el {} con número {}""".format(row["Tipo documento"], row["Número de documento"])  
            answer = ' - '
        finally:
            lista_CIF.at[row_index, 'Búsqueda encontrada'] = answer
            lista_CIF.at[row_index, 'Detalles cliente'] = detalles
            navigate_to_customer_searcher(browser=browser, username=username, password=password)
            print('El {} con número {} ha sido procesado\n'.format(row['Tipo documento'], row['Número de documento']))
    
    print('Se han procesado todos los clientes. El archivo ya está actualizado con los datos')

else:
    
    print("""
             Se ha encontrado un error tipo {} en el fichero.\n
             Por favor, reviselo antes de ejecutar el programa de nuevo
          """.format(check_file(lista_CIF))   )
        
browser.close()

#Se deslocalizan los detalles de cliente, organizando cada fila en una columna distinta
lista_CIF = split_customer_details(lista_CIF)
#Se exportan los datos 
export_new_file(lista_CIF, path)

