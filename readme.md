Robot

## Install dependencies

pip install -r requirements.txt

## Freeze dependencies

pip freeze > requirements.txt

## selenium driver

Right-click the start menu and choose Run.
In the Run window, type winver and press OK.
The window that opens will display the Windows 10 build that is installed.
Microsoft Edge
WebDriver for Microsoft Edge will work with the stable channel and all insider channels
Download the correct Microsoft WebDriver version for your build of Microsoft Edge.

To find your correct build number: Launch Microsoft Edge. Open the Settings and more (...) menu, choose Help and feedback, and then choose About Microsoft Edge. Having the correct version of WebDriver for your build ensures it runs correctly.

Download a WebDriver language binding of your choice. All Selenium language bindings support Microsoft Edge.
Download a testing framework of your choice.

download https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/
